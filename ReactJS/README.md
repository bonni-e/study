## [리액트JS 개발자 스터디 (노마드 10주 완성)](https://nomadcoders.co/react-study)
**스터디 기간 : 2023년 2월 6일 ~ 4월 17일 (10주)**

#### 학습하는 강의
55시간. 5개 강의 | ReactJS, Typescript, NextJS

1. [ReactJS 기초](https://gitlab.com/bonni-e/study/-/tree/main/ReactJS/ReactJS-basic) (영화 웹 서비스 만들기)
2. [Typescript 기초](https://gitlab.com/bonni-e/study/-/tree/main/ReactJS/Typescript) (Typescript로 블록체인 만들기)
3. [ReactJS 마스터클래스](https://gitlab.com/bonni-e/study/-/tree/main/ReactJS/ReactJS-master)
4. ~~NextJS 기초 (NextJS 시작하기)~~
5. ~~[풀스택] 캐럿마켓 클론코딩~~

#### 포트폴리오
- 무비 앱
- 넷플릭스 클론
- 마블 앱
- 트위터 클론
