## [Curriculum](https://nomadcoders.co/react-for-beginners/lobby)

**Day 01** (1/30)

1. INTRODUCTION
2. THE BASICS OF REACT
3. STATE

**Day 02** (2/1)

4. PROPS
5. CREATE REACT APP
6. EFFECTS

**Day 03** (2/2)

7. PRACTICE MOVIE APP

**Day 04** (2/3)

8. INTRODUCTION
9. SETUP
10. JSX & PROPS
11. STATE

**Day 05** (2/4)

12. MAKING THE MOVIE APP

**Day 06** (2/5)

13. CONCLUSIONS
14. ROUTING BONUS
